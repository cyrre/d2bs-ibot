;
; Pickit format: <stat> <operator> <value> [: <pickit format>]
; 'count' can be used as stat, counts items that matches same line (item to be picked is counted aswell)
;
; ex: name = jewel : quality = magic : requirementpercent <= -15 : count <= 1
;