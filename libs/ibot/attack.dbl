js_strict(true);

include('ibot/skills.dbl');

var Attack = new function() {
	this.Mode = {Norm:0,Pre:1,Post:2};
	var preAttacks = [];
	var untimedAttacks = [];
	var timedAttacks = [];
	var postAttacks = [];
	
	this.Init = function Init() {
		Interface.Display(Message.Info,'Initializing attack manager');
		var dir = dopen("/libs/attacks/");
		var files = dir.getFiles();
		for each(let file in files) {
			var i = file.indexOf(".");
			if (i == -1) continue;
			Interface.Display(Message.DDebug, 'Including attack sequence \'' + file + '\'.');
			include('attacks/' + file);
		}
		
		var sortFunc = function(a,b){return (b.Skill.level - a.Skill.level);}
		preAttacks.sort(sortFunc);
		untimedAttacks.sort(sortFunc);
		timedAttacks.sort(sortFunc);
		postAttacks.sort(sortFunc);
	};
	
	this.addAttack = function addAttack(attack) {
		attack.Mode = attack.Mode || Attack.Mode.Norm;
		if (attack.Skill.level < 1) return;
		switch(attack.Mode) {
			case Attack.Mode.Norm:
				if (attack.Skill.delay > 0) timedAttacks.push(attack);
				else untimedAttacks.push(attack);
				break;
			case Attack.Mode.Pre: preAttacks.push(attack); break;
			case Attack.Mode.Post: postAttacks.push(attack); break;
		}
	};
	
	var checkSkills = function checkSkills(list, unit) {
		var ret = [];
		for each(let a in list)
			if (a.Predicate(unit)) ret.push(a);
		return ret;
	};
	
	this.Range = function Range(range, point, callback) {
		point = point || me;
		callback = callback || function(){return true;};
		if (!isPoint(point)) throw new Error('Invalid point provided');
		Pather.MoveTo(point);
		var ms = Unit.findMonsters().filter(function(x){return getDistance(point,x)<range;});
		var vis;
		if (Interface.Config('General','Visuals',true))
			vis = new Circle(point.x,point.y,range,98);
		for each(let u in ms) {
			this.Kill(u);
			if (!callback()) break;
		}
		if (vis) vis.remove();
	};
	
	this.ClearRect = function ClearRect(rect, callback) {
		callback = callback || function(){return true;};
		var vis;
		if (Interface.Config('General','Visuals',true))
			vis = new RectangleHook(rect.x,rect.y,rect.width,rect.height,98);
		var ms = Unit.findMonsters().filter(function(x){return inRect(rect,x);});
		for each(let u in ms) {
			this.Kill(u);
			if (!callback()) break;
		}
		if (vis) vis.remove();
	};
	
	this.ClearPath = function ClearPath(point, callback) {
		callback = callback || function(){return true;};
		Pather.MoveTo(point,function(p){Attack.Range(25,p,callback);return true;});
	};
	
	this.ClearArea = function ClearArea(area, callback) {
		area = area || me.area;
		callback = callback || function(){return true;};
		Pather.MoveToArea(area);
		var rooms = toArray(getRooms(area), function(o){return {x:o.realx,y:o.realy};});
		rooms.sort(function(x,y){return getDistance(me,x)-getDistance(me,y);});
		var vis;
		if (Interface.Config('General','Visuals',true))
			vis = new PathHook(rooms);
		for(var i=0;i<rooms.length;i++) {
			try {
				this.ClearPath(rooms[i], callback);
				if (!callback()) break;
			}
			catch(err) {
				print('ERR: '+err.message);
			}
			if (vis) vis.removeFirst();
		}
		if (vis) vis.Remove();
	};
	
	this.Kill = function Kill(unit, callback) {
		if (!isUnit(unit)) throw new Error('Called with invalid unit.');
		if (!unit.isValidTarget) return;
		callback = callback || function(){return true;};
		
		if (Interface.Config('General','Visuals',true))
			load('libs/screenhooks/targethook.dbl',unit.gid,200,300);
		
		var pre = checkSkills(preAttacks, unit);
		var untimed = checkSkills(untimedAttacks, unit);
		var timed = checkSkills(timedAttacks, unit);
		
		if (untimed.length < 1 && timed.length < 1)
			throw new Error('No skills to kill '+unit.name+' with.');
		
		for each(let a in pre) a.Func(unit);
		
		while(unit.isValidTarget) {
			for each(let a in timed) {
				if (!a.lastCastTime || ((new Date()).getTime() - a.lastCastTime > a.Skill.delay)) {
					a.Func(unit);
					a.lastCastTime = (new Date()).getTime();
				}
				break;
			}
			for each(let a in untimed) { a.Func(unit); delay(150); break; }
			callback();
		}
		
		var post = checkSkills(postAttacks, unit);
		for each(let a in post) a.Func(unit);
	};
};