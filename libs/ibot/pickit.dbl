js_strict(true);

include('ibot/storage.dbl');
include('common/nip.dbl');

var Pickit = new function() {
	this.buckets = [];
	this.buckets['misc'] = [];
	this.buckets['name'] = [];
	this.buckets['type'] = [];
	
	this.Init = function Init() {
		Interface.Display(Message.Info,'Initializing pickit manager');
		var pickitFiles = Interface.Config('Pickit','Files',[]);
		for each(let file in pickitFiles) {
			var data = NipParser.ReadFile('pickit/'+file+'.nipx', this.translateData);
			for each(var line in data)
				this.AddData(line);
			Interface.Display(Message.Debug,'Loaded pickit file ('+file+'), '+data.length+' records added');
		}
	};
	
	this.AddLine = function AddLine(str) {
		for each(let d in NipParser.ReadString(str, this.translateData))
			this.AddData(d);
	};
	this.AddData = function AddData(data) {
		var type = data.stats.filter(function(x){return x.stat[0] == 'type';});
		var name = data.stats.filter(function(x){return x.stat[0] == 'code';});
		if (type.length > 0) {
			var t = type[0].value;
			if (!this.buckets['type'][t]) this.buckets['type'][t] = [];
			this.buckets['type'][t].push(data);
		}
		else if (name.length > 0) {
			var n = name[0].value;
			if (!this.buckets['name'][n]) this.buckets['name'][n] = [];
			this.buckets['name'][n].push(data);
		}
		else
			this.buckets['misc'].push(data);
	}
	
	this.translateData = function translateData(stat, val) {
		stat = [stat.toLowerCase(),0];
		val = val.toLowerCase();
		switch(stat[0]) {
			case 'type':
			case 'code':
			case 'classid':
			case 'count': break;
			case 'quality':
				if (!PickitQuality.hasOwnProperty(val)) throw new Error('Failed to translate pickit quality value, '+val);
				val = PickitQuality[val]; break;
			case 'flag': val = PickitFlag[val]; break;
			case 'name':
				stat[0] = 'code';
				if (!PickitCodes.hasOwnProperty(val)) throw new Error('Failed to translate pickit name value, '+val);
				val = PickitCodes[val]; break;
			default:
				if (PickitStats.hasOwnProperty(stat[0])) stat = PickitStats[stat[0]];
				else throw new Error('Failed to translate pickit stat, '+stat);
				break;
		}
		return {stat:stat, value:val};
	};
	
	this.Pick = function Pick() {
		for each(let item in Unit.findItems({mode:Mode.Item.Group.Ground}))
			if (Pickit.checkItem(item))
				Pickit.pickItem(item);
		return true;
	};
	
	this.pickItem = function pickItem(item /*item || gid*/) {
		if (isNumber(item)) item = getUnit(Type.Item, null, null, item);
		if (!isItem(item)) throw new Error('Called with invalid argument, could not get item');
		
		Interface.Display(Message.Info, 'Picking item, '+item.formattedName);
		
		if (!Storage.Inventory.CanFit(item) &&
			Town.Return(Town.Tick) &&
			!Storage.Inventory.CanFit(item)) {
				Interface.Display(Message.Warning, 'Item does not fit in inventory, cant pick ('+item.formattedName+')');
				return false;
		}
		dropDelay(item);
		return item.pick();
	};
	
	this.checkFastPick = function checkFastPick(item /*item || gid*/) {
		if (isNumber(item)) item = getUnit(Type.Item, null, null, item);
		if (!isItem(item)) throw new Error('Called with invalid argument, could not get item.');
		
		return false;
	};
	this.checkItem = function checkItem(item /*item || gid*/, strict) {
		if (isNumber(item)) item = getUnit(Type.Item, null, null, item);
		if (!isItem(item)) throw new Error('Called (checkItem) with invalid argument, could not get item.');

		Interface.Display(Message.DDebug, 'Checking item for picking, '+item.formattedName);
		var list = this.buckets['misc'];
		list = list.concat((this.buckets['type'][getBaseStat(BaseStatTable.itemtypes,item.itemType,'code')] || []));
		list = list.concat((this.buckets['name'][item.code] || []));

		for each(let line in list)
			if (this.checkItemLine(item, line, strict)) return true;
		return false;
	};
	this.checkItemLine = function checkItemLine(item /*item || gid*/, line, strict, count) {
		if (isNumber(item)) item = getUnit(Type.Item, null, null, item);
		if (!isItem(item)) throw new Error('Called (checkItemLine) with invalid argument, could not get item.');
		count = count != undefined ? count : true;

		if (!line.hasOwnProperty('stats')) {
			Interface.Display(Message.Warning, 'Undefined property \'stats\' ('+line.toSource()+')');
			return false;
		}

		for each(let check in line.stats) {
			var val;
			if (!check.hasOwnProperty('stat')) {
				Interface.Display(Message.Warning, 'Undefined property \'stat\' ('+line.toSource()+')');
				return false;
			}
			switch(check.stat[0]) {
				case 'type': val = getBaseStat(BaseStatTable.itemtypes,item.itemType,'code'); break;
				//case 'name': val = item.name.toLowerCase().substring(item.prefix?item.prefix.length:0,item.suffix?item.suffix.length:item.name.length); print('PICKITNAME: \''+val+'\''); break;
				case 'code': val = item.code; break;
				case 'classid': val = item.classid; break;
				case 'quality': val = item.quality; break;
				case 'flag': val = hasFlag(item.flags, check.value) ? check.value : -1; break;
				case 'count':
					if (!count) continue;
					val = Storage.Items.filter(
							function(x){return Pickit.checkItemLine(x, line, strict, false);}
						).length;// + item.location == ItemLocation.Ground ? 1 : 0;
					val += item.location == ItemLocation.Ground ? 1 : 0;
					break;
				default:
					if (isNaN(check.stat[0])) throw new Error('Unrecognised stat, '+check.stat[0]);
					var stat = Number(check.stat[0]);
					if (!item.isIdentified && !strict) continue;
					//if (!item.isIdentified) throw new Error('Item need to be identified to perform strict check.');
					switch(stat) {
						case Stats.SingleSkill: case Stats.AddSkillTab: case Stats.AddClassSkills:
							val = item.givesSkillBonus(check.stat[0], check.stat[1]);
							break;
						default:
							val = item.getStat(check.stat[0], check.stat[1]);
							break;
					}
					break;
			}
			switch(check.op) {
				case '=':
				case '==': if (val != check.value) return false; break;
				case '!=':
				case '<>': if (val == check.value) return false; break;
				case '<': if (val >= check.value) return false; break;
				case '>': if (val <= check.value) return false; break;
				case '<=': if (val > check.value) return false; break;
				case '>=': if (val < check.value) return false; break;
			}
		}
		Interface.Display(Message.Debug, item.name+' passed pickit check ('+(strict?'':'not ')+'strict), matched against line '+line.line+' in file '+line.file);
		return true;
	};
};